// Copyright © 2020 Brian Hooper <knowntraveler.io>
// Author: Brian Hooper (@KnownTraveler)
// Project: gitlab.com/KnownTraveler/golang-cli/

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package main

import (
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/KnownTraveler/golang-cli/log"
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "sampl",
	Short: "Sampl is a Boilerplate CLI utility project",
	Long: `
SAMPL COMMAND LINE UTILITY BOILERPLATE

# AUTHOR
Brian Hooper (@KnownTraveler) https://knowntraveler.io

# LICENSE
The SAMPL-CLI Source Code is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/. 

_______________________________________________________________________________

SUMMARY

SAMPL is designed to be a lightweight boilerplate project for quickly bootstrapping
new Command Line Interface Projects using Golang

SAMPL is a command line utility written in Go using Cobra/Viper, 
which are libraries for Go that empower command line applications.
- Cobra https://github.com/spf13/cobra
- Viper https://github.com/spf13/viper
`,

	PersistentPreRun: func(cmd *cobra.Command, args []string) {

		// Configure Verbose Logging Option from CLI
		if options.verbose {
			log.EnableVerbose()
		}

		// Configure Logging options from Environment
		logLevel := os.Getenv("SAMPL_DEBUG")

		if logLevel == "verbose" {
			log.EnableVerbose()
		}

		if logLevel == "debug" {
			log.EnableVerbose()
			log.EnableDebug()
		}

		if logLevel == "trace" {
			log.EnableVerbose()
			log.EnableDebug()
			log.EnableTrace()
		}

	},
}

type rootFlags struct {
	verbose              bool
	nonInteractive       bool
	overrideConfirmation bool
}

var options rootFlags

func init() {
	// Commands
	rootCmd.AddCommand(CmdVersion)

	// PersistentFlags
	rootCmd.PersistentFlags().BoolVarP(&options.verbose, "verbose", "v", false, "Enable verbose output")
	rootCmd.PersistentFlags().BoolVar(&options.nonInteractive, "non-interactive", false, "Disable interactive prompts")
	rootCmd.PersistentFlags().BoolVarP(&options.overrideConfirmation, "yes", "y", false, "Override confirmations")

}
