// Copyright © 2020 Brian Hooper <knowntraveler.io>
// Author: Brian Hooper (@KnownTraveler)
// Project: gitlab.com/KnownTraveler/golang-cli/

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package main

import (
	"github.com/spf13/cobra"
	"gitlab.com/KnownTraveler/golang-cli/log"
	"gitlab.com/KnownTraveler/golang-cli/version"
)

// CmdVersion represents the version command
var CmdVersion = &cobra.Command{
	Use:   "version",
	Short: "Version is a command...",
	Long:  `Version is a command that prints the version information of SAMPL.`,
	Run:   runVersion,
}

// RunVersion Command
func runVersion(cmd *cobra.Command, _ []string) {
	log.Printf("sampl-cli %s\n", version.Version)
}
