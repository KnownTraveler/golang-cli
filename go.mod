module gitlab.com/KnownTraveler/golang-cli

go 1.15

require (
	github.com/AlecAivazis/survey/v2 v2.1.1
	github.com/logrusorgru/aurora v2.0.3+incompatible
	github.com/mitchellh/go-homedir v1.1.0
	github.com/onsi/ginkgo v1.14.0
	github.com/spf13/cobra v1.0.0
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.3.0
)
