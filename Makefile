BIN="./bin"
SRC=$(shell find . -name "*.go")


.PHONY: test

default: all

all: clean fmt test build

clean:
	$(info ********** cleaning ************)
	rm -rf ./bin/

fmt:
	$(info ********** formatting **********)
	gofmt -s -l -w $(SRC)

test:
	$(info ********** testing *************)
	go test ./... -v

build:
	$(info ********** building ************)
	mkdir ./bin
	go build -o $(BIN) ./cmd/sampl

