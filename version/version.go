// Copyright © 2020 Brian Hooper <knowntraveler.io>
// Author: Brian Hooper (@KnownTraveler)
// Project: gitlab.com/KnownTraveler/golang-cli/

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

// Package log (gitlab.com/KnownTraveler/golang-cli/log) provides a uniform
// Logging API that has been customized for the CLI to print
// messages to the user via stdout
package version

// Version is the GOLANG CLI version. Overridden vwith ldflags at build time
var Version = "v0.0.1 (alpha)"
